# Domain Portals

Public portals for domain or project-specific data at the
[ESRF](https://www.esrf.fr/):

- [Human Organ Atlas](https://human-organ-atlas.esrf.fr/)

## Developing

To build and preview the website locally, install [pnpm](https://pnpm.io/) and
then run

```sh
pnpm install
pnpm build
pnpm preview:hoa
```

The last command can be changed to `pnpm preview:paleo` to serve the paleo
website instead.
