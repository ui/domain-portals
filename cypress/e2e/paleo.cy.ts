describe('Paleo', () => {
  beforeEach(() => {
    cy.visit('/');
    cy.waitForStableDOM();
  });

  it('render homepage', () => {
    cy.findByRole('heading', {
      name: 'Welcome to the Paleontology database',
    }).should('be.visible');
  });
});
