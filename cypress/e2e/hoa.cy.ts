describe('Human Organ Atlas', () => {
  beforeEach(() => {
    cy.visit('/');
    cy.waitForStableDOM();
  });

  it('render homepage', () => {
    cy.findByRole('heading', {
      name: 'Welcome to the Human\u00A0Organ\u00A0Atlas',
    }).should('be.visible');
  });
});
