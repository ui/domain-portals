export interface Filter {
  name: string;
  label: string;
  type: string;
  hideUnit?: boolean;
  placeholder?: string;
  step?: number;
}

export interface GroupFilter {
  groupLabel: string;
  filters: Filter[];
}
