import DateFilter from './DateFilter';
import type { Filter } from './filters-models';
import ListPicker from './ListPicker';
import OptionsPicker from './OptionsPicker';
import Range from './Range';
import TextInput from './TextInput';

function FilterRouting(props: Filter) {
  const { type, ...filterProps } = props;

  switch (type) {
    case 'text':
      return <TextInput {...filterProps} />;

    case 'radios':
      return <OptionsPicker {...filterProps} />;

    case 'checkboxes':
      return <ListPicker {...filterProps} />;

    case 'range':
      return <Range {...filterProps} />;

    case 'date':
      return <DateFilter {...filterProps} />;

    default:
      throw new Error(`Unspported filter type: ${type}`);
  }
}

export default FilterRouting;
