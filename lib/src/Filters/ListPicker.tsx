import { useMetadataValues } from 'lib/api-hooks';

import { capitalizeFirstLetter } from '../helpers';
import FilterBox from './FilterBox';
import styles from './Pickers.module.css';
import { useListQueryParam } from './router-utils';

interface Props {
  name: string;
  label: string | boolean;
}

function ListPicker(props: Props) {
  const { name, label } = props;
  const param = useListQueryParam(name);

  const valuesParam = useMetadataValues(name);

  const listPicks = valuesParam.filter((p) => p !== ' ');

  return (
    <FilterBox
      title={label}
      showTitle={label !== false}
      isActive={param.isActive}
      onClear={() => param.remove()}
    >
      <div className={styles.listOption}>
        {listPicks.map((item) => (
          <label className={styles.option} key={item}>
            <input
              className={styles.inputOption}
              type="checkbox"
              checked={param.values.includes(item)}
              name={item}
              aria-label={item}
              onChange={() => param.toggleValue(item)}
            />
            <div className={styles.labelOption}>
              {capitalizeFirstLetter(item)}
            </div>
          </label>
        ))}
      </div>
    </FilterBox>
  );
}

export default ListPicker;
