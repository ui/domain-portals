import react from '@vitejs/plugin-react-swc';
import { defineConfig } from 'vite';
import { patchCssModules } from 'vite-css-modules';
import { checker } from 'vite-plugin-checker';
import eslintPlugin from 'vite-plugin-eslint';

export default defineConfig({
  server: { open: !process.env.CI },
  plugins: [
    react(),
    patchCssModules(),
    { ...eslintPlugin(), apply: 'serve' }, // dev only to reduce build time
    { ...checker({ typescript: true }), apply: 'serve' }, // dev only to reduce build time
  ],
});
