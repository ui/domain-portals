import type { ProcessedDataset } from 'lib/api-models';
import { assertEnvVar } from 'lib/guards';
import { capitalizeFirstLetter, getMetadataByName } from 'lib/helpers';
import { Link, useSearch } from 'wouter';
import { useBrowserLocation } from 'wouter/use-browser-location';

import styles from './DatasetCard.module.css';

interface Props {
  dataset: ProcessedDataset;
  sessionId: string;
}

function DatasetCard(props: Props) {
  const { id, parameters, gallery } = props.dataset;
  const { sessionId } = props;

  const [location] = useBrowserLocation();
  const queryParams = useSearch();
  const backURL = queryParams ? [location, queryParams].join('?') : location;

  const previousId = window.history.state?.previousId;

  const ICATP = import.meta.env.VITE_ICATP;
  assertEnvVar(ICATP, 'VITE_ICATP');

  const url = `~/datasets/${encodeURIComponent(id)}`;
  const thumbnail = gallery.find((f) => f.type === 'image');

  const PILLS_PARAMS = [
    'SamplePaleo_scientific_domain',
    'SampleLocalisation_continental_region',
    'SampleLocalisation_country',
    'SamplePaleoGeologicalTime_era',
    'SamplePaleoGeologicalTime_period',
  ];

  return (
    <div>
      <Link
        className={styles.datasetCard}
        to={url}
        state={{ backLabel: 'results', backURL }}
        data-previousid={id === Number(previousId) || undefined}
      >
        <div className={styles.datasetContent}>
          <h2 className={styles.datasetTitle}>
            {getMetadataByName(parameters, 'datasetName')}
          </h2>
          <div className={styles.datasetInfo}>
            {PILLS_PARAMS.map((name) => {
              const metadata = getMetadataByName(parameters, name);
              if (metadata === undefined) {
                return null;
              }
              return (
                <div className={styles.infoPill} key={metadata}>
                  {capitalizeFirstLetter(metadata)}
                </div>
              );
            })}
          </div>
          <p className={styles.datasetAbstract}>
            {getMetadataByName(parameters, 'DOI_abstract')}
          </p>
        </div>
        <div className={styles.thumbnail}>
          {thumbnail && (
            // Put loading="lazy" before src. See comment in lib/Dataset/Media.tsx
            <img
              alt=""
              loading="lazy"
              src={`${ICATP}/resource/${sessionId}/file/download?resourceId=${thumbnail.id}`}
            />
          )}
        </div>
      </Link>
    </div>
  );
}

export default DatasetCard;
