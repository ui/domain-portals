import type { ProcessedDataset } from 'lib/api-models';
import { DOI, MetaItem } from 'lib/dataset';
import { capitalizeFirstLetter, getMetadataByName } from 'lib/helpers';
import type { PropsWithChildren } from 'react';

import styles from './Metadata.module.css';

interface Props {
  doi: string;
  dataset: ProcessedDataset;
}

function Metadata(props: PropsWithChildren<Props>) {
  const { doi, dataset, children } = props;

  const comment = getMetadataByName(dataset.parameters, 'Notes_note_00');
  const beamline = getMetadataByName(dataset.parameters, 'beamlineID');
  const species = getMetadataByName(
    dataset.parameters,
    'SamplePaleoClassification_species',
  );
  const scientificDomain = getMetadataByName(
    dataset.parameters,
    'SamplePaleo_scientific_domain',
  );

  return (
    <>
      <div className={styles.metadataSection}>
        <div className={styles.headingMaster}>
          <h2 className={styles.headingTitle}>Description</h2>
          <p className={styles.headingAbstract}>
            {getMetadataByName(dataset.parameters, 'DOI_abstract')}
          </p>
          {comment && (
            <div className={styles.comment}>
              Comment:{' '}
              {comment.split('\n').map((c) => (
                <p key={c}>{c}</p>
              ))}
            </div>
          )}
        </div>
        <ul className={styles.metadataList}>
          <MetaItem label="Scientific domain">
            {scientificDomain && capitalizeFirstLetter(scientificDomain)}
          </MetaItem>
          <MetaItem label="Repository institution">
            {getMetadataByName(
              dataset.parameters,
              'SamplePaleo_repository_institution',
            )}
          </MetaItem>
          <MetaItem label="DOI">
            <DOI doi={doi} />
          </MetaItem>
          <MetaItem label="Reference">
            {getMetadataByName(
              dataset.parameters,
              'ExternalReferencesPublication_endnote',
            )}
          </MetaItem>
          <MetaItem label="Segmentation">
            {getMetadataByName(dataset.parameters, 'Notes_note_01')}
          </MetaItem>
          <MetaItem label="Instrument">
            {beamline || dataset.instrumentName}, ESRF
          </MetaItem>
        </ul>
      </div>

      {children}

      <div className={styles.metadataSection}>
        <div className={styles.heading}>
          <h2 className={styles.headingTitle}>Location</h2>
        </div>
        <ul className={styles.metadataList}>
          <MetaItem label="Continental region">
            {getMetadataByName(
              dataset.parameters,
              'SampleLocalisation_continental_region',
            )}
          </MetaItem>
          <MetaItem label="Country">
            {getMetadataByName(
              dataset.parameters,
              'SampleLocalisation_country',
            )}
          </MetaItem>
          <MetaItem label="Localisation">
            {getMetadataByName(dataset.parameters, 'SampleLocalisation_name')}
          </MetaItem>
          <MetaItem label="Formation">
            {getMetadataByName(
              dataset.parameters,
              'SamplePaleoGeologicalTime_formation',
            )}
          </MetaItem>
        </ul>
      </div>
      <div className={styles.metadataSection}>
        <div className={styles.heading}>
          <h2 className={styles.headingTitle}>Geological Time</h2>
        </div>
        <ul className={styles.metadataList}>
          <MetaItem label="Era">
            {getMetadataByName(
              dataset.parameters,
              'SamplePaleoGeologicalTime_era',
            )}
          </MetaItem>
          <MetaItem label="Period">
            {getMetadataByName(
              dataset.parameters,
              'SamplePaleoGeologicalTime_period',
            )}
          </MetaItem>
          <MetaItem label="Epoch">
            {getMetadataByName(
              dataset.parameters,
              'SamplePaleoGeologicalTime_epoch',
            )}
          </MetaItem>
        </ul>
      </div>
      <div className={styles.metadataSection}>
        <div className={styles.heading}>
          <h2 className={styles.headingTitle}>Classification</h2>
        </div>
        <ul className={styles.metadataList}>
          {[1, 2, 3, 4, 5, 6].map((index) => {
            const clade = getMetadataByName(
              dataset.parameters,
              `SamplePaleoClassification_clade${index}`,
            );
            return (
              <MetaItem key={index} label={`Clade ${index}`}>
                {clade}
              </MetaItem>
            );
          })}
          <MetaItem label="Species">{species && <em>{species}</em>}</MetaItem>
        </ul>
      </div>
    </>
  );
}

export default Metadata;
