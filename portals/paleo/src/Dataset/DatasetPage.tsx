import { useDatasetsById, useDOI } from 'lib/api-hooks';
import { Files, Gallery } from 'lib/dataset';
import styles from 'lib/DatasetPage.module.css';
import { assertDefined } from 'lib/guards';
import { getMetadataByName, trackDownload } from 'lib/helpers';
import { usePageTracking, useScrollToTop } from 'lib/hooks';
import { Helmet } from 'react-helmet';

import stylesPaleo from './DatasetPage.module.css';
import Metadata from './Metadata';

interface Props {
  datasetId: string;
  isDesktop: boolean;
}

function handleClick(evt: React.MouseEvent<HTMLAnchorElement>) {
  trackDownload(evt);
}

function DatasetPage(props: Props) {
  usePageTracking();
  useScrollToTop();

  const { isDesktop, datasetId } = props;
  assertDefined(datasetId);

  const [dataset] = useDatasetsById([datasetId]);
  assertDefined(dataset);

  const doi = useDOI(dataset.id);

  return (
    <>
      <Helmet>
        <title>{getMetadataByName(dataset.parameters, 'DOI_title')}</title>
      </Helmet>
      <div>
        <h1 className={styles.heading}>
          {getMetadataByName(dataset.parameters, 'DOI_title')}
        </h1>
        <div className={styles.content}>
          <div className={styles.metadata}>
            <Metadata doi={doi} dataset={dataset}>
              {!isDesktop && (
                <div className={stylesPaleo.scienceFiles}>
                  <Gallery files={dataset.gallery} />
                  <Files
                    doi={doi}
                    datasetId={datasetId}
                    handleClick={handleClick}
                  />
                </div>
              )}
            </Metadata>
          </div>
          {isDesktop && (
            <div className={styles.scienceFiles}>
              <Gallery files={dataset.gallery} />
              <Files
                doi={doi}
                datasetId={datasetId}
                handleClick={handleClick}
              />
            </div>
          )}
        </div>
      </div>
    </>
  );
}

export default DatasetPage;
