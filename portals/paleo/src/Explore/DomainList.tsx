import { useMetadataValues } from 'lib/api-hooks';

import DomainCard from './DomainCard';
import styles from './DomainList.module.css';

function DomainList() {
  const domainList = useMetadataValues('SamplePaleo_scientific_domain');

  return (
    <div className={styles.domainContainer}>
      {domainList.map((domain) => (
        <DomainCard key={domain} domain={domain} />
      ))}
    </div>
  );
}

export default DomainList;
