import { ExploreLayer } from 'lib/explore-layout';
import { usePageTracking } from 'lib/hooks';
import { Helmet } from 'react-helmet';
import { Route } from 'wouter';

import DatasetList from './DatasetList';
import DomainList from './DomainList';
import SampleList from './SampleList';

function ExplorePage() {
  usePageTracking();

  return (
    <>
      <Helmet>
        <title>Explore</title>
      </Helmet>
      <div>
        <Route path="/" nest>
          <ExploreLayer title="Domains">
            <DomainList />
          </ExploreLayer>
        </Route>
        <Route path="/:domains" nest>
          <ExploreLayer title="Samples">
            <SampleList />
          </ExploreLayer>
        </Route>
        <Route path="/:domains/:samples" nest>
          <ExploreLayer title="Datasets">
            <DatasetList />
          </ExploreLayer>
        </Route>
      </div>
    </>
  );
}

export default ExplorePage;
