import { capitalizeFirstLetter } from 'lib/helpers';
import { Link, useRoute } from 'wouter';

import styles from './DomainCard.module.css';

interface Props {
  domain: string;
}

function DomainCard(props: Props) {
  const { domain } = props;

  const [isActive] = useRoute(`/${domain}/*?`);

  return (
    <Link
      key={domain}
      className={styles.domainCard}
      to={`/${encodeURIComponent(domain)}`}
      data-active={isActive || undefined}
    >
      <div>
        <h2 className={styles.domainCardTitle}>
          {capitalizeFirstLetter(domain)}
        </h2>
      </div>
    </Link>
  );
}

export default DomainCard;
