import { useMetadataValues } from 'lib/api-hooks';
import { assertDefined } from 'lib/guards';
import { useScrollIntoView } from 'lib/hooks';
import { useParams } from 'wouter';

import SampleCard from './SampleCard';
import styles from './SampleList.module.css';

function SampleList() {
  const { domains } = useParams();
  assertDefined(domains);
  const queryParam = `SamplePaleo_scientific_domain~eq~${domains}`;
  const scrollRef = useScrollIntoView(domains);

  const sampleUnique = useMetadataValues('Sample_name', [queryParam]);

  return (
    <div ref={scrollRef} className={styles.sampleContainer}>
      {sampleUnique.map((sample) => (
        <SampleCard key={sample} sample={sample} />
      ))}
    </div>
  );
}

export default SampleList;
