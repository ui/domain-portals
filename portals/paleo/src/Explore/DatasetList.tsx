import { useDatasetsByQuery, useIcatSessionId } from 'lib/api-hooks';
import styles from 'lib/Explore/DatasetList.module.css';
import { assertDefined } from 'lib/guards';
import { useScrollIntoView } from 'lib/hooks';
import { useParams } from 'wouter';

import DatasetCard from '../Search/DatasetCard';

function DatasetList() {
  const sessionId = useIcatSessionId();
  const { domains, samples } = useParams();
  assertDefined(domains);
  assertDefined(samples);

  const scrollRef = useScrollIntoView(samples);

  const queryParam = `SamplePaleo_scientific_domain~eq~${domains},Sample_name~eq~${samples}`;

  const datasets = useDatasetsByQuery(queryParam);

  return (
    <div ref={scrollRef} className={styles.datasetContainer}>
      {datasets.map((d) => (
        <DatasetCard key={d.id} dataset={d} sessionId={sessionId} />
      ))}
    </div>
  );
}

export default DatasetList;
