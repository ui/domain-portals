import { Link, useLocation } from 'wouter';

import styles from './SampleCard.module.css';

interface Props {
  sample: string;
}

function SampleCard(props: Props) {
  const { sample } = props;

  // Having some blank spaces in the path seems to break the behaviour
  // of useRoute and it doesn't match the route anymore.
  // With blank spaces, useLocation returns the raw path
  // (e.g.: '~/explore/vertebrate paleontology', instead of
  // just '/vertebrate paleontology') but we can still split the
  // domain or sample out in any case.
  const [location] = useLocation();
  const splitLocation = location.split('/');
  const sampleURL = splitLocation[splitLocation.length - 1];

  const isActive = sample === sampleURL;

  return (
    <Link
      key={sample}
      className={styles.sampleCard}
      to={`/${encodeURIComponent(sample)}`}
      data-active={isActive || undefined}
    >
      <div>
        <h2 className={styles.sampleCardTitle}>{sample}</h2>
      </div>
    </Link>
  );
}

export default SampleCard;
