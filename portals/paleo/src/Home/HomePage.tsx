import { usePageTracking, useScrollToTop } from 'lib/hooks';

import europeanFlag from '../images/Flag_of_Europe.png';
import styles from './HomePage.module.css';
import VideoCarousel from './VideoCarousel';

function HomePage() {
  usePageTracking();
  useScrollToTop();

  return (
    <div className={styles.container}>
      <section className={styles.section}>
        <h1 className={styles.headTitle}>
          Welcome to the Paleontology database
        </h1>
        <p>
          The Paleontology database hosts data obtained from X-ray Computed
          Tomography experiments at ESRF. Started in 2011, it has helped the
          ESRF user community share their data upon publication.
        </p>
        <p>
          While the ESRF data portal hosts the raw data from the experiments,
          this space provides access to processed tomograms and, in some cases,
          associated derivatives (images, movies, 3D meshes). Data are only
          deposited here upon request from the experimental teams and relevant
          parties (museum curators, researchers involved in the analysis).
          Revamped in 2024, each dataset is now associated with geographical,
          geological, temporal, and taxonomical keywords to facilitate searches.
        </p>
        <p>
          Data hosted here cover a wide taxonomical spectrum and can represent
          complete specimens or specific close-ups depending on the case.
          Tomograms (i.e., the stack of virtual slices) are stored as JPEG2000
          to help with storage space and facilitate download by users. We hope
          that this open-access database, enabled by ESRF-EBS, will contribute
          to the reuse of data according to the FAIR principles.
        </p>
      </section>

      <section className={styles.section}>
        <VideoCarousel />
      </section>

      <section className={styles.section}>
        <h2 className={styles.sectionTitle}>Acknowledgements</h2>
        <p className={styles.acknowledgementParagraph}>
          The development of this portal has been done as part of the{' '}
          <a
            className={styles.inlineLink}
            href="https://streamline.esrf.fr/"
            target="_blank"
            rel="noreferrer"
          >
            STREAMLINE project.
          </a>
        </p>
        <div className={styles.acknowledgementBox}>
          <img
            className={styles.europeanFlag}
            src={europeanFlag}
            alt="European flag"
          />
          <p className={styles.acknowledgementParagraph}>
            STREAMLINE has received funding from the European Union’s{' '}
            <a
              className={styles.inlineLink}
              href="https://ec.europa.eu/programmes/horizon2020/en/what-horizon-2020"
              target="_blank"
              rel="noreferrer"
            >
              Horizon 2020
            </a>{' '}
            research and innovation programme under grant agreement No. 870313.
          </p>
        </div>
        <p>
          The following people were involved in the development: Vincent
          Fernandez, Paul Tafforeau, Alejandro De Maria Antolinos, Axel
          Bocciarelli, Marjolaine Bodin, Guillaume Gaisné and Andrew Götz from
          the ESRF, as well as the broader STREAMLINE and ICAT communities.
        </p>
      </section>
    </div>
  );
}

export default HomePage;
