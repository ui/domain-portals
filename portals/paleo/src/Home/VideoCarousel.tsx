import { Carousel } from 'lib/home';
import { useState } from 'react';
import { Link, useLocation } from 'wouter';

import styles from './VideoCarousel.module.css';
import { PALEO_CAROUSEL_VIDEOS } from './videos';

export default function VideoCarousel() {
  const VIDEO_PARAM = '&autoplay=0&mute=1&loop=1';
  const VIDEOS_COUNT = PALEO_CAROUSEL_VIDEOS.length;
  const randomVideoNumber = Math.floor(Math.random() * VIDEOS_COUNT);
  const [videoNumber, setVideoNumber] = useState(randomVideoNumber);
  const videoParameter = PALEO_CAROUSEL_VIDEOS[videoNumber];
  const { url } = videoParameter;
  const [location] = useLocation();
  function changeSlide(index: number) {
    setVideoNumber((VIDEOS_COUNT + videoNumber + index) % VIDEOS_COUNT);
  }

  return (
    <Carousel changeSlide={changeSlide}>
      <div className={styles.carouselContainer}>
        <div className={styles.videoContainer}>
          <iframe
            className={styles.videoPlayer}
            src={url + VIDEO_PARAM}
            title={`Youtube video about ${videoParameter.title}`}
            allow="fullscreen; accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
            referrerPolicy="strict-origin-when-cross-origin"
          />
        </div>

        <div className={styles.captionContainer}>
          <p>{videoParameter.title}.</p>
          <p>
            See the{' '}
            <Link
              className={styles.inlineLink}
              href={`/datasets/${videoParameter.datasetId}`}
              state={{ backLabel: 'home', backURL: location }}
            >
              related dataset page
            </Link>
            . {videoParameter.caption}{' '}
            <a
              className={styles.inlineLink}
              href={videoParameter.link}
              target="_blank"
              rel="noreferrer"
            >
              ESRF YouTube channel
            </a>
            .
          </p>
        </div>
      </div>
    </Carousel>
  );
}
