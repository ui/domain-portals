const BASIC_CAPTION = 'More videos can be found on';

const ESRF_YOUTUBE_CHANNEL = 'https://www.youtube.com/@ESRFsynchrotron';

export const PALEO_CAROUSEL_VIDEOS = [
  {
    title: 'Heart position inside Gogo fish fossil. Credits: Alice Clement',
    url: 'https://www.youtube.com/embed/KEbegynicxE?si=W7CoU0IA-U5GMmg1&playlist=KEbegynicxE',
    caption: BASIC_CAPTION,
    link: ESRF_YOUTUBE_CHANNEL,
    datasetId: 1_634_315_670,
  },
  {
    title: 'The ESRF unveils a 2000 year old Egyptian crocodile mummy',
    url: 'https://www.youtube.com/embed/svpdO1ZkADE?si=4pSldUFjBpk6nTfn&playlist=svpdO1ZkADE',
    caption: BASIC_CAPTION,
    link: ESRF_YOUTUBE_CHANNEL,
    datasetId: 1_634_389_628,
  },
  {
    title: 'Halszkaraptor in 3D',
    url: 'https://www.youtube.com/embed/HzShBB1oX6w?si=ymKoisjeRObKKA-L&playlist=HzShBB1oX6w',
    caption: 'More context of the Halszkaraptor on',
    link: 'https://www.youtube.com/playlist?list=PLsWatK2_NAmzKTUnsHFmhv5dBipdd7aJm',
    datasetId: 1_634_316_283,
  },
  {
    title: 'Anguimorph Thai Embryo HD',
    url: 'https://www.youtube.com/embed/1KH97wP8nQ8?si=eiKhy9DlgwRpEUkv&playlist=1KH97wP8nQ8',
    caption: BASIC_CAPTION,
    link: ESRF_YOUTUBE_CHANNEL,
    datasetId: 1_634_315_827,
  },
];
