import { NavBar, NavLink } from 'lib/navbar';

interface Props {
  isDesktop: boolean;
}

function Navigation(props: Props) {
  const { isDesktop } = props;

  return (
    <NavBar isDesktop={isDesktop} title="Paleontology database">
      <NavLink to="/explore">Explore</NavLink>
      <NavLink to="/search">Search</NavLink>
      <NavLink to="/help">Help</NavLink>
    </NavBar>
  );
}

export default Navigation;
