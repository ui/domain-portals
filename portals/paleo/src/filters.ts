import type { GroupFilter } from 'lib/filters-models';

export const FILTERS_BY_GROUP: GroupFilter[] = [
  {
    groupLabel: 'Keywords',
    filters: [
      {
        name: 'Keywords',
        label: 'Authors, clades, epochs, ...',
        type: 'text',
        placeholder: 'France, museum, ...',
      },
    ],
  },
  {
    groupLabel: 'Scientific domain',
    filters: [
      {
        name: 'SamplePaleo_scientific_domain',
        label: 'Discipline',
        type: 'checkboxes',
      },
    ],
  },
  {
    groupLabel: 'Geological time',
    filters: [
      {
        name: 'SamplePaleoGeologicalTime_era',
        label: 'Era',
        type: 'checkboxes',
      },
    ],
  },
  {
    groupLabel: 'Localisation',
    filters: [
      {
        name: 'SampleLocalisation_continental_region',
        label: 'Continent',
        type: 'checkboxes',
      },
    ],
  },
];
