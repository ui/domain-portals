import { assertNonNull } from 'lib/guards';
import styles from 'lib/TableOfContent.module.css';
import { useLocation } from 'wouter';

function TableOfContent() {
  const [, navigate] = useLocation();

  function handleSectionLinkClick(event: React.MouseEvent<HTMLAnchorElement>) {
    event.preventDefault();
    const to = event.currentTarget.getAttribute('href');
    assertNonNull(to);
    navigate(`${to}`);
    document.querySelector(to)?.scrollIntoView({ behavior: 'smooth' });
  }

  return (
    <div className={styles.tocList}>
      <div>
        <a
          className={styles.linkHelp}
          href="#q1"
          onClick={handleSectionLinkClick}
        >
          How to download data from the Paleontology database
        </a>
      </div>
      <div>
        <a
          className={styles.linkHelp}
          href="#q2"
          onClick={handleSectionLinkClick}
        >
          What to do if the downloaded ZIP file is corrupt ?
        </a>
      </div>
      <div>
        <a
          className={styles.linkHelp}
          href="#q3"
          onClick={handleSectionLinkClick}
        >
          Under what license are the data from the Paleontology database made
          available ?
        </a>
      </div>
      <div>
        <a
          className={styles.linkHelp}
          href="#q4"
          onClick={handleSectionLinkClick}
        >
          How to cite the data from the Paleontology database ?
        </a>
      </div>
      <div>
        <a
          className={styles.linkHelp}
          href="#q5"
          onClick={handleSectionLinkClick}
        >
          How to search for data in the Paleontology database ?
        </a>
      </div>
      <div>
        <a
          className={styles.linkHelp}
          href="#q6"
          onClick={handleSectionLinkClick}
        >
          Who to contact in case of problem with the Paleontology database ?
        </a>
      </div>
    </div>
  );
}

export default TableOfContent;
