import { usePageTracking, useScrollToTop } from 'lib/hooks';
import stylesToc from 'lib/TableOfContent.module.css';
import { Helmet } from 'react-helmet';

import downloadAll from '../images/download-all.png';
import downloads from '../images/downloads.png';
import endpointResults from '../images/endpoint-results.png';
import firefoxRetry from '../images/firefox-retry.png';
import globusDownload from '../images/globus-download.png';
import styles from './HelpPage.module.css';
import TableOfContent from './TableOfContent';

interface Props {
  isDesktop: boolean;
}

function HelpPage(props: Props) {
  usePageTracking();
  useScrollToTop();

  const { isDesktop } = props;
  return (
    <>
      <Helmet>
        <title>Help</title>
      </Helmet>
      <div className={styles.wrapper}>
        {isDesktop ? (
          <div className={stylesToc.tocAside}>
            <div className={stylesToc.tocSticky}>
              <h1 className={stylesToc.tocTitle}>Help</h1>
              <TableOfContent />
            </div>
          </div>
        ) : (
          <details className={stylesToc.tocDetails}>
            <summary className={stylesToc.tocSummary}>Help</summary>
            <div className={stylesToc.tocContent}>
              <TableOfContent />
            </div>
          </details>
        )}
        <div>
          <section id="q1">
            <h2 className={styles.sectionTitle}>
              How to download data from the Paleontology database ?
            </h2>
            <p className={styles.sectionParagraph}>
              Once you have found the dataset you want to download, you will see
              a list of files displayed like this:
            </p>
            <div>
              <img
                className={styles.image}
                src={downloads}
                alt="Screenshot of a list of download links as it may appear on a dataset page"
              />
            </div>
            <p className={styles.paragraph}>
              You have two options for downloading the files:
            </p>
            <ul>
              <li>
                <h3 className={styles.subsectionTitle}>
                  Download individual files with HTTP
                </h3>
                <p className={styles.sectionParagraph}>
                  This is the simplest solution. Download the file you are
                  interested in by simply clicking on the name of the file. This
                  will start downloading the file using the standard web
                  protocol HTTP, which requires no extra tools to use. The
                  downside of this approach is that the file transfer protocol
                  is not reliable for big files and can fail after a few
                  gigabytes depending on your internet connection. In case of
                  failure, your browser will indicate this in the downloads box
                  or toolbar &mdash; e.g. in Firefox, if you click on the{' '}
                  <em>Downloads</em> button, you may see something like this:
                </p>
                <div>
                  <img
                    className={styles.image}
                    src={firefoxRetry}
                    alt="Screenshot of a failed download in Firefox, with a retry button on the right-hand side"
                  />
                </div>
                <p className={styles.sectionParagraph}>
                  In this case, you can continue downloading the file by
                  clicking on the retry button (i.e. the clockwise arrow). The
                  download will resume from where it failed. You may have to
                  repeat this process multiple times for very large files (i.e.
                  tens of gigabytes). A more efficient and reliable way to
                  download large files as well as multiple files at once is to
                  use Globus.
                </p>
              </li>

              <li>
                <h3 className={styles.subsectionTitle}>
                  Download multiple files with Globus
                </h3>
                <p className={styles.sectionParagraph}>
                  This is the best option for downloading large datasets
                  reliably.{' '}
                  <a
                    className={styles.inlineLink}
                    href="https://www.globus.org/"
                    target="_blank"
                    rel="noreferrer"
                  >
                    Globus
                  </a>{' '}
                  is a service for downloading and transferring files. In order
                  to use Globus to download files to your computer, you first
                  need to{' '}
                  <a
                    className={styles.inlineLink}
                    href="https://www.globus.org/globus-connect-personal"
                    target="_blank"
                    rel="noreferrer"
                  >
                    install Globus Connect Personal
                  </a>
                  . Once installed, start the software, and log in to create a{' '}
                  <a
                    className={styles.inlineLink}
                    href="https://docs.globus.org/faq/globus-connect-endpoints/"
                    target="_blank"
                    rel="noreferrer"
                  >
                    local endpoint
                  </a>
                  . In the search box, type "ESRF Paleo" to find the database'
                  public endpoint:
                </p>
                <div>
                  <img
                    className={styles.image}
                    src={endpointResults}
                    alt="Screenshot showing the Paleontology database endpoint search result"
                  />
                </div>
                <p className={styles.sectionParagraph}>
                  Click on the endpoint and then click on{' '}
                  <em>Open in File Manager</em>, you should see all the datasets
                  organised by samples. Now select the dataset(s) you want to
                  download. Note that you can select all the files in the
                  current folder from the file manager's top bar:
                </p>
                <div>
                  <img
                    className={styles.image}
                    src={downloadAll}
                    alt="Screenshot showing the Globus file manager's top bar with the checkbox to select all files in the current folder"
                  />
                </div>
                <p className={styles.sectionParagraph}>
                  Select the directory in which you want the data to be
                  downloaded and start the file transfer by clicking on the{' '}
                  <em>Start</em> button. Globus will restart the transfer
                  automatically in case of failure and inform you once it is
                  complete.
                </p>
                <p className={styles.sectionParagraph}>
                  From a dataset's page, you can go directly to the dataset's
                  folder on Globus by clicking on the{' '}
                  <em>Download with Globus</em> button:
                </p>
                <div>
                  <img
                    className={styles.image}
                    src={globusDownload}
                    alt="Screenshot showing a Globus download link on a dataset page"
                  />
                </div>
                <p className={styles.sectionParagraph}>
                  In case of problems, please email the ESRF data managers at{' '}
                  <a
                    className={styles.inlineLink}
                    href="mailto:dataportalrequests@esrf.fr"
                    target="_blank"
                    rel="noreferrer"
                  >
                    dataportalrequests@esrf.fr
                  </a>
                  .
                </p>
              </li>
            </ul>
          </section>

          <section id="q2">
            <h2 className={styles.sectionTitle}>
              What to do if the downloaded ZIP file is corrupt ?
            </h2>
            <p className={styles.sectionParagraph}>
              If you have downloaded a ZIP file and your operating system cannot
              open it because it is corrupt, then most likely the download was
              performed with HTTP. Use Globus as explained above to try to
              download the ZIP file again in a more reliable way.
            </p>
          </section>

          <section id="q3">
            <h2 className={styles.sectionTitle}>
              Under what license are the data from the Paleontology database
              made available ?
            </h2>
            <p className={styles.sectionParagraph}>
              The data are made available under the Creative Commons Attribution
              Noncommercial CC-BY-NC-4.0 license. This means the data are
              accessible by all for re-use as long as the publication and DOI
              are cited when they are re-used and/or new publications are
              published which have used the data.
            </p>
          </section>

          <section id="q4">
            <h2 className={styles.sectionTitle}>
              How to cite the data from the Paleontology database ?
            </h2>
            <p className={styles.sectionParagraph}>
              Any publications or re-use of the data from the Paleontology
              database must cite the DOI and reference attached to each dataset.
            </p>
          </section>

          <section id="q5">
            <h2 className={styles.sectionTitle}>
              How to search for data in the Paleontology database ?
            </h2>
            <p className={styles.sectionParagraph}>
              Click on the <em>Search</em> tab and select the filter settings
              you are interested in to find the datasets which match the
              filters.
            </p>
          </section>

          <section id="q6">
            <h2 className={styles.sectionTitle}>
              Who to contact in case of problem with the Paleontology database ?
            </h2>
            <ul>
              <li>
                For technical issues with downloading the data or using the data
                portal, please email{' '}
                <a
                  className={styles.inlineLink}
                  href="mailto:dataportalrequests@esrf.fr"
                  target="_blank"
                  rel="noreferrer"
                >
                  dataportalrequests@esrf.fr
                </a>
                .
              </li>
              <li>
                For questions about the data and how they were processed, please
                contact the authors of the publications.
              </li>
            </ul>
          </section>
        </div>
      </div>
    </>
  );
}

export default HelpPage;
