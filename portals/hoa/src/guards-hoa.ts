import type { ReconstructedOrgan } from './Reconstructions/ReconstructionsPage';

export function assertReconstOrgan(
  val: string,
  array: Record<string, unknown>,
): asserts val is ReconstructedOrgan {
  if (!Object.keys(array).includes(val)) {
    throw new Error(
      `Expected reconstructed organ, ${val} is not reconstructed`,
    );
  }
}
