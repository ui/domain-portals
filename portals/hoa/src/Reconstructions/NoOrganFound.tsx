import styles from './NoOrganFound.module.css';

interface Props {
  organ: string | undefined;
}

function NoOrganFound(props: Props) {
  const { organ } = props;

  return (
    <>
      <h1 className={styles.reconstructionTitle}>
        Interactive 3D reconstructions
      </h1>
      <div className={styles.errorMessage}>
        {organ
          ? `No such organ (${organ}) has been 3D reconstructed yet.`
          : 'Enter a valid organ name in the URL.'}
      </div>
    </>
  );
}

export default NoOrganFound;
