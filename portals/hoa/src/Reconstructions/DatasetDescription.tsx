import { useDatasetsById, useDOI } from 'lib/api-hooks';
import { MetaItem } from 'lib/dataset';
import { getMetadataByName, trackLink } from 'lib/helpers';
import { FiExternalLink } from 'react-icons/fi';
import { Link } from 'wouter';

import styles from './DatasetDescription.module.css';

interface Props {
  datasetId: string;
}

function DatasetDescription(props: Props) {
  const { datasetId } = props;
  const [dataset] = useDatasetsById([datasetId]);
  const doi = useDOI(dataset.id);

  return (
    <>
      <div className={styles.descriptionContainer}>
        <h2 className={styles.descriptionTitle}>Description</h2>
        <p className={styles.descriptionAbstract}>
          {getMetadataByName(dataset.parameters, 'DOI_abstract')}
        </p>
        <p className={styles.datasetSource}>
          The data was produced on{' '}
          <a
            className={styles.inlineLink}
            href="https://mecheng.ucl.ac.uk/hip-ct/"
            target="_blank"
            rel="noreferrer"
          >
            UCL
          </a>
          -led beamtimes md1252/1290 at the{' '}
          <a
            className={styles.inlineLink}
            href="https://www.esrf.fr/"
            target="_blank"
            rel="noreferrer"
          >
            ESRF
          </a>
          , funded in part by the{' '}
          <a
            className={styles.inlineLink}
            href="https://chanzuckerberg.com"
            target="_blank"
            rel="noreferrer"
          >
            CZI
          </a>
          .
        </p>
      </div>
      <ul className={styles.metadataContainer}>
        <MetaItem label="Dataset" narrow>
          <Link
            className={styles.doiTitle}
            to={`~/datasets/${datasetId}`}
            state={{ backLabel: 'reconstructions' }}
          >
            {getMetadataByName(dataset.parameters, 'DOI_title')}
          </Link>
        </MetaItem>
        <MetaItem label="DOI" narrow>
          <a
            className={styles.linkDOI}
            href={`http://doi.org/${doi}`}
            target="_blank"
            rel="noreferrer"
            onClick={(evt) => trackLink(evt)}
          >
            <span>DOI</span>
            <span>{doi}</span>
            <span>
              <FiExternalLink />
            </span>
          </a>
        </MetaItem>
        <MetaItem label="Technique" narrow>
          {getMetadataByName(dataset.parameters, 'TOMO_technique')}
        </MetaItem>
        <MetaItem label="Instrument" narrow>
          {dataset.instrumentName}, ESRF
        </MetaItem>
      </ul>
    </>
  );
}

export default DatasetDescription;
