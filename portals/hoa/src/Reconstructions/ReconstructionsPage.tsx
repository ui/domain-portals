// Due to Interspectral being a proprietary software, a license is required
// to stream their products. The actual license has not been renewed and so
// the streaming is broken. This component and the ones imported herein are
// therefore useless for the website and are then not rendered with the
// 3D reconstructions page route being shut.
// Still, they are kept Headers, in the possibility of a renewal for that
// license in the future.

import { Boundary } from 'lib/boundary';
import { usePageTracking, useScrollToTop } from 'lib/hooks';
import { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { useLocation, useRoute } from 'wouter';

import { assertReconstOrgan } from '../guards-hoa';
import DatasetDescription from './DatasetDescription';
import NoOrganFound from './NoOrganFound';
import OrganLink from './OrganLink';
import styles from './ReconstructionsPage.module.css';

export type ReconstructedOrgan = 'lung' | 'brain';

interface Reconstruction {
  datasetId: string;
  embedUrl: string;
}

const RECONSTRUCTIONS = {
  lung: {
    datasetId: '571998122',
    embedUrl:
      'https://cloud.interspectral.com/viewerEmbedded/10/1/3667d1ac804e3bfa62e8090d3ec222dd0fca6634',
  },
  brain: {
    datasetId: '572252538',
    embedUrl:
      'https://cloud.interspectral.com/viewerEmbedded/10/0/8566fb5fcd360da3fb27cf3e557c82834cdecfb7',
  },
} satisfies Record<ReconstructedOrgan, Reconstruction>;

function ReconstructionsPage() {
  useScrollToTop();
  usePageTracking();

  const [, params] = useRoute('/:organ');
  const [, navigate] = useLocation();

  const organ = params?.organ;

  useEffect(() => {
    if (!organ) {
      navigate('/lung');
    }
  }, [organ, navigate]);

  if (!organ || !Object.keys(RECONSTRUCTIONS).includes(organ)) {
    return <NoOrganFound organ={organ} />;
  }

  assertReconstOrgan(organ, RECONSTRUCTIONS);
  return (
    <>
      <Helmet>
        <title>3D Reconstructions</title>
      </Helmet>

      <h1 className={styles.reconstructionTitle}>
        Interactive 3D reconstructions
      </h1>
      <p className={styles.reconstructionAbstract}>
        Explore 3D visualizations of high-resolution{' '}
        <a
          className={styles.inlineLink}
          href="https://mecheng.ucl.ac.uk/hip-ct/"
          target="_blank"
          rel="noreferrer"
        >
          <abbr title="Hierarchical Phase-Contrast Tomography">HiP-CT</abbr>
        </a>{' '}
        scans from the Human Organ Atlas &mdash; a partnership with Swedish
        company{' '}
        <a
          className={styles.inlineLink}
          href="https://interspectral.com/"
          target="_blank"
          rel="noreferrer"
        >
          Interspectral
        </a>
        &trade;, bridging the gap between science and outreach.
      </p>

      <div className={styles.reconstructionContainer}>
        <div className={styles.choiceContainer}>
          <div className={styles.embeddedChoiceContainer}>
            <OrganLink
              organ={organ}
              choiceOrgan="lung"
              title="Complete left lung upper lobe"
            />
            <OrganLink
              organ={organ}
              choiceOrgan="brain"
              title="Complete brain"
            />
          </div>
        </div>

        <div className={styles.iframeContainer}>
          <div className={styles.embeddedIframeContainer}>
            <iframe
              className={styles.iframe}
              title="interspectral-frame"
              src={RECONSTRUCTIONS[organ].embedUrl}
              allowFullScreen
              allow="fullscreen; accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            />
          </div>
        </div>
        <div>
          <Boundary resetKeys={[]}>
            <DatasetDescription datasetId={RECONSTRUCTIONS[organ].datasetId} />
          </Boundary>
        </div>
      </div>
    </>
  );
}

export default ReconstructionsPage;
