import { FiActivity, FiPlay } from 'react-icons/fi';
import { Link } from 'wouter';

import styles from './OrganLink.module.css';

interface Props {
  organ: string;
  choiceOrgan: string;
  title: string;
}

function OrganLink(props: Props) {
  const { organ, choiceOrgan, title } = props;

  return (
    <Link
      className={styles.pill}
      to={`/${choiceOrgan}`}
      {...(organ === choiceOrgan ? { 'data-alt3': '' } : { 'data-alt': '' })}
    >
      <span />
      <span>{title}</span>
      <span>{organ === choiceOrgan ? <FiActivity /> : <FiPlay />}</span>
    </Link>
  );
}

export default OrganLink;
