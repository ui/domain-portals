import { trackDownload } from 'lib/helpers';
import { Modal } from 'react-responsive-modal';

import type { ModalInfo } from '../models';
import { useCitationStore } from '../stores';
import styles from './ModalDownload.module.css';

const LICENSE_NAME = import.meta.env.VITE_LICENSE_NAME;
const LICENSE_URL = import.meta.env.VITE_LICENSE_URL;

interface Props {
  modalState: ModalInfo;
  setModalState: (arg0: ModalInfo) => void;
}

function ModalDownload(props: Props) {
  const { setModalSeen } = useCitationStore();
  const { modalState, setModalState } = props;

  return (
    <Modal
      center
      blockScroll={false}
      open={modalState.open}
      onClose={() => {
        setModalState({ ...modalState, open: false });
      }}
    >
      <h2 className={styles.modalTitle}>Citation required</h2>
      <p className={styles.intro}>
        When using this data, please cite the following publication and DOI:
      </p>
      <p className={styles.citation}>
        Walsh, C.L., Tafforeau, P., Wagner, W.L. <em>et al.</em> Imaging intact
        human organs with local resolution of cellular structures using
        hierarchical phase-contrast tomography. <em>Nat Methods</em> (2021).
        https://doi.org/10.1038/s41592-021-01317-x
      </p>
      <p className={styles.dataPolicy}>
        These datasets are made freely available under the{' '}
        <a
          className={styles.infoLink}
          href={LICENSE_URL}
          target="_blank"
          rel="noreferrer"
        >
          {LICENSE_NAME} licence
        </a>
        .
      </p>
      <div className={styles.downloadInfos}>
        <a
          className={styles.downloadLink}
          href={modalState?.url}
          download={modalState?.filename}
          target="_blank"
          rel="noreferrer"
          onClick={(evt) => {
            trackDownload(evt);

            if (modalState.ok) {
              setModalSeen(true);
            }

            setModalState({ ...modalState, open: false });
          }}
        >
          {modalState.url?.includes('globus')
            ? 'Continue to Globus'
            : 'Download file'}
        </a>
        <div className={styles.downloadAgreement}>
          <input
            id="ok"
            className={styles.agreementCheckbox}
            type="checkbox"
            name="download agreement checkbox"
            aria-label="download agreement checkbox"
            onChange={() => {
              setModalState({ ...modalState, ok: true });
            }}
          />
          <label htmlFor="ok" className={styles.agreementLabel}>
            I understand, don't show this again
          </label>
        </div>
      </div>
    </Modal>
  );
}

export default ModalDownload;
