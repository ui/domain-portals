export interface ModalInfo {
  open: boolean;
  url: string;
  ok: boolean;
  filename: string;
}
